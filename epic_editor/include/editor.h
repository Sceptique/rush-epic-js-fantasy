/*
** editor.h for  in /home/ghukas_g/rendu/colle/rush-epic-js-fantasy/epic_editor
**
** Made by ghukas_g
** Login   <ghukas_g@epitech.net>
**
** Started on  Fri May  9 22:19:46 2014 ghukas_g
** Last update Sat May 10 03:12:20 2014 ghukas_g
*/

#ifndef			EDITOR_H_
# define		EDITOR_H_

# define		MAX_LEGTH 20

# define		NOPARAM 0
# define		STRING 1
# define		VALUE 2

typedef struct		s_tab
{
  char			name[MAX_LEGTH + 1];
  int			id;
  int			mode;
}			t_tab;

extern t_tab		g_tab[];

void			my_putfchar(char c, int fd);
void			my_putfstr(char *str, int fd);
void			my_putfnbr(int size, int fd);
char			**trans_tab(char *str);
int			write_with_arg(char *flag, char *str, int fd);
int			write_champ(char **tab, int fd);
int			write_monster(char **tab, int fd);
int			write_room(char **tab, int fd);

#endif			/* !EDITOR_H_ */
