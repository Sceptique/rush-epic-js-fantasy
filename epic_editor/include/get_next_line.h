/*
** get_next_line.h for get_next_line in /home/ghukas_g/rendu/get_next_line-2018-ghukas_g
**
** Made by ghukas_g
** Login   <ghukas_g@epitech.net>
**
** Started on  Sat Nov 16 17:19:38 2013 ghukas_g
** Last update Wed Dec  4 12:56:59 2013 ghukas_g
*/

#ifndef GET_NEXT_LINE_H_
 #define GET_NEXT_LINE_H_

#define SIZE_BUFFER 5
char *get_next_line(const int);

#endif /* !GET_NEXT_LINE_H_ */
