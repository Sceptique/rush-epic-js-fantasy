/*
** exit_error.c for  in /home/ghukas_g/rendu/colle/rush-epic-js-fantasy/epic_editor
**
** Made by ghukas_g
** Login   <ghukas_g@epitech.net>
**
** Started on  Sat May 10 01:22:58 2014 ghukas_g
** Last update Sat May 10 01:51:57 2014 ghukas_g
*/

#include	<stdlib.h>
#include	<string.h>

void		exit_error(char *str)
{
  if (str != NULL)
    write(2, str, strlen(str));
  exit(EXIT_FAILURE);
}
