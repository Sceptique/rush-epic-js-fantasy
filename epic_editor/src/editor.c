/*
** main.c for  in /home/ghukas_g/rendu/colle/rush-epic-js-fantasy/epic_editor
**
** Made by ghukas_g
** Login   <ghukas_g@epitech.net>
**
** Started on  Fri May  9 22:17:01 2014 ghukas_g
** Last update Fri May  9 23:38:24 2014 ghukas_g
*/

#include	<stdlib.h>
#include	"editor.h"

t_tab		g_tab[] =
  {
    {"NAME", 0x01, STRING},
    {"ROOM_TO_WIN", 0x02, STRING},
    {"ROOM_TO_START", 0x03, STRING},
    {"TYPE", 0x04, STRING},
    {"HP", 0x05, VALUE},
    {"SPEED", 0x06, VALUE},
    {"DEG", 0x07, VALUE},
    {"WEAPON", 0x08, STRING},
    {"ARMOR", 0x09, STRING},
    {"ADV", 0x10, STRING},
    {"TAB=>CONNECTION", 0x11, STRING},
    {"TAB=>MONSTER", 0x12, STRING},
    {"ROOM", 0x0F, STRING},
    {"SPE", 0x20, VALUE},
    {"CHAMPION", 0x0C, NOPARAM},
    {"HEADER", 0x0D, NOPARAM},
    {"MONSTER", 0x0E, NOPARAM},
    {"SEP_SECTION", 0x0A, NOPARAM},
    {"MAGIC_NUMBER", 0x123, NOPARAM},
    {0, 0, 0}
  };
