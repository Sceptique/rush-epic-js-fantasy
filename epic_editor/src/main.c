/*
** main.c for  in /home/ghukas_g/rendu/colle/rush-epic-js-fantasy/epic_editor
**
** Made by ghukas_g
** Login   <ghukas_g@epitech.net>
**
** Started on  Fri May  9 22:39:55 2014 ghukas_g
** Last update Sun May 11 21:25:25 2014 ghukas_g
*/

#include	<sys/types.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<stdio.h>
#include	<string.h>
#include	<stdlib.h>
#include	"editor.h"
#include	"get_next_line.h"

static int	write_header(int fd)
{
  char		*str;
  char		**tab;
  int		i;

  if ((str = get_next_line(0)) == NULL)
    return (-1);
  tab = trans_tab(str);
  my_putfnbr(123, fd);
  i = 0;
  while (tab[i] != NULL)
    i = i + 1;
  if (i != 4  || strcmp("HEADER", tab[0]) != 0)
    exit_error("Bad format of header\n");
  my_putfnbr(find_id_tab("HEADER"), fd);
  write_with_arg("NAME", tab[1], fd);
  write_with_arg("ROOM_TO_WIN", tab[2], fd);
  write_with_arg("ROOM_TO_START", tab[3], fd);
  return (0);
}

char		**init_fonc(int (*sect[3])(char **tab, int fd), int fd)
{
  char		**tab;

  sect[0] = &write_champ;
  sect[1] = &write_monster;
  sect[2] = &write_room;
  tab = trans_tab(strdup("[Champ][Monster][ROOM]"));
  if (tab == NULL)
    perror("Malloc error");
  write_header(fd);
  return (tab);
}

void		main(int ac, char **av)
{
  int		fd;
  char		**tab;
  int		(*sect[3])(char **tab, int fd);
  char		**sect_tab;
  int		i;

  if ((fd = open(ac > 1 ? av[1] : strdup("game_file"),
		 O_CREAT | O_WRONLY | O_TRUNC, 00777)) < 0)
    perror("Open error");
  sect_tab = init_fonc(sect, fd);
  while ((tab = trans_tab(get_next_line(0))) != NULL)
    {
      i = -1;
      while (++i < 3 && tab != NULL)
	if (strcmp(tab[0], sect_tab[i]) == 0)
	  {
	    my_putfnbr(find_id_tab("SEP_SECTION"), fd);
	    sect[i](tab, fd);
	    free(tab);
	    tab = NULL;
	  }
      if (i > 3)
	exit_error("Bad input");
    }
  close(fd);
}
