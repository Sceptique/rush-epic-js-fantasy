/*
** my_str_to_wordtab.c for XPisicne in /home/exam/sujet/misc
**
** Made by
** Login   <exam@epitech.net>
**
** Started on  Mon Apr  7 18:44:06 2014
** Last update Sun May 11 22:06:09 2014 stefan-l
*/

#include <stdlib.h>

int	chars_check(char c, char *chars)
{
  int	i;
  static int id = 0;

  i = 0;
  if (c == '"')
    id = (id == 1) ? (0) : (1);
  if (id == 1)
    return (-1);
  while (chars[i] != 0)
    {
      if (c == chars[i])
	return (0);
      i++;
    }
  return (-1);
}

int	init_str_to_wordtab(char *str, char *chars, char ***tab)
{
  int	i;
  int	nb_lines;

  i = 0;
  nb_lines = 2;
  while (str[i] != 0)
    {
      if (chars_check(str[i], chars) == 0)
	nb_lines = nb_lines + 1;
      i++;
    }
  if ((*tab = malloc(sizeof(char*) * nb_lines)) == NULL)
    exit(-1);
  return (nb_lines);
}

void	inc_line(int *i, int *long_line, char *str, char *chars)
{
  *long_line = 0;
  while (str[*i] != 0 && chars_check(str[*i], chars) != 0)
    {
      ++(*long_line);
      ++(*i);
    }
  *i = *i - *long_line;
}

char	**my_str_to_wordtab(char *str, char *chars)
{
  int	i;
  int	j;
  int	long_line;
  int	nb_lines;
  int	line;
  char	**tab;

  nb_lines = init_str_to_wordtab(str, chars, &tab);
  i = 0;
  line = 0;
  while (str[i] != 0)
    {
      j = 0;
      inc_line(&i, &long_line, str, chars);
      if ((tab[line] = malloc(sizeof(char) * (long_line + 1))) == NULL)
	exit(-1);
      while (str[i] != 0 && chars_check(str[i], chars) != 0)
	if (str[i++] != '"')
	  tab[line][j++] = str[i - 1];
      tab[line++][j] = 0;
      if (str[i] != 0)
	i++;
    }
  tab[line] = NULL;
  return (tab);
}
