/*
** initiative.c for  in /home/poulet_a/projets/rushfantasy/server
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Sat May 10 17:37:40 2014 poulet_a
** Last update Sun May 11 21:55:57 2014 poulet_a
*/

#include <stdlib.h>
#include "structs.h"

/* TODO : duplicate this function
/*
** get the better initiative under init_max
** if init_max == 1, return the better initiative
*/
t_monster	*get_max_speed(t_monster *monsters)
{
  int		speed;
  int		i;

  speed = 0;
  i = 0;
  while (monsters[i] != NULL)
    {
      if (monsters[i]->speed >= speed)
	{
	  speed = monsters[i]->speed;
	}
      ++i;
    }
  return (speed);
}
