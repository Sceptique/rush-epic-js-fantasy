/*
** client_handle.c for  in /home/poulet_a/projets/jsfantasy
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Sat May 10 01:40:17 2014 poulet_a
** Last update Sun May 11 22:49:49 2014 poulet_a
*/

#include <stdlib.h>
#include <unistd.h>
#include "get_next_line.h"
#include "network.h"
#include "server.h"

char		**get_cmd()
{
  char		*cmd;
  char		**cmd_splited;

  if ((cmd = get_next_line(1)) == NULL)
    return (NULL);
  if ((cmd_splited = my_str_to_word_tab(cmd)) == NULL)
    return (NULL);
  return (cmd_splited);
}

/* TODO Trouver une connection avec la salle qui a un nom qui match */
/* Deplacer le pointeur de la room */
/* si un monstre est présent, combat */
static int	play_map(t_champ *champ, t_room **rooms)
{
  int		i;
  char		**cmd;

  i = 0;
  while (rooms[i]->position != ROOM_END && champ->hp > 0)
    {
      if (room_is_empty(rooms[i]))
	{
	  if ((cmd = get_cmd()) == NULL)
	    return (ERR_MALLOC);
	}
      else
	{
	  fight(champ, rooms[i]);
	}
    }
  return (0);
}

/* static void	accept_clients(t_champ *champs) */
/* { */
/*   t_champ       *client; */

/*   client = champs; */
/*   while (client != NULL) */
/*     { */
/*       /\* accept and put the adress in client *\/ */
/*       client = client->next; */
/*     } */
/* } */
/* accept_clients(champs); */
/* get rooms and monsters */
/* TODO : SE POSITIONNER SUR ROOM_START */
int		run_server(int server_fd, char *map_name)
{
  t_champ	*champs;
  t_room	**rooms;

  champs = NULL;
  rooms = NULL;
  if (load_map(&champs, &rooms, map_name) != 0)
    return (ERR_LOAD_MAP);
  return (play_map(champs, rooms));
}
