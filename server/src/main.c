/*
** main.c for  in /home/poulet_a/projets/jsfantasy/client
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Sat May 10 00:01:22 2014 poulet_a
** Last update Sun May 11 22:51:09 2014 poulet_a
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdio.h>
#include "network.h"
#include "server.h"

/* int			get_socket() */
/* { */
/*   int			server_fd = 0; */
/*   struct sockaddr_in	sock_addr; */
/*   server_fd = socket(AF_INET, SOCK_STREAM, 0); */
/*   sock_addr.sin_family = AF_INET; */
/*   sock_addr.sin_addr.s_addr = htonl(INADDR_ANY); */
/*   sock_addr.sin_port = htons(SERVER_PORT); */
/*   bind(server_fd, (struct sockaddr*)(&sock_addr), sizeof(sock_addr)); */
/*   return (server_fd); */
/* } */
/* if ((server_fd = get_socket()) == -1) */
/*   { */
/*     printf("Error : cannot build the socket\n"); */
/*     return (-2); */
/*   } */
/* if (listen(server_fd, MAX_BACKLOG) == -1) */
/*   { */
/*     printf("Error : cannot listen\n"); */
/*     return (-3); */
/*   } */
int	main(int argc, char **argv)
{
  int	server_fd;

  if (argc < 2)
    {
      printf("Error : no map filename specified\n");
      return (-1);
    }
  return (run_server(server_fd, argv[1]));
}
