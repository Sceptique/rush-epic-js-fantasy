/*
** network.h for  in /home/poulet_a/projets/jsfantasy/server
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Sat May 10 00:07:55 2014 poulet_a
** Last update Thu May  8 11:33:11 2014 poulet_a
*/

#ifndef NETWORK_H_
# define NETWORK_H_

# define THREAD_FAIL	-876
# define SERVER_LISTEN	"0.0.0.0"
# define SERVER_PORT	4224
# define MAX_BACKLOG	4

#endif /* !NETWORK_H_ */
