/*
** print_cmds.c for rpg in /home/harnay_a/rendu/CPE_2013_Rush2/client
** 
** Made by harnay_a
** Login   <harnay_a@epitech.net>
** 
** Started on  Fri May  9 22:47:55 2014 harnay_a
** Last update Sat May 10 00:21:26 2014 harnay_a
*/

#include <stdio.h>
#include "client.h"

void	print_next(char *room_name)
{
  printf("\"You moved into %s\"\n", room_name);
}

void	print_list_team(t_champ *champ)
{
  while (champ != NULL)
    {
      printf("Champ%i\n", champ->id);
      printf("Name:%s\n", champ->name);
      printf("Type:%s\n", champ->type);
      printf("HP:%i\n", champ->hp);
      printf("SPE:%i\n", champ->spe);
      printf("SPEED:%i\n", champ->speed);
      printf("WEAPON:%s\n", champ->weapon);
      printf("ARMOR:%s\n", champ->armor);
      champ = champ->next;
    }
}

void	print_list_monster(t_monster *monster)
{
  while (monster != NULL)
    {
      printf("Monster%i\n", monster->id);
      printf("Type:%s\n", monster->type);
      printf("HP:%i\n", monster->hp);
      printf("SPE:%i\n", monster->spe);
      printf("SPEED:%i\n", monster->speed);
      printf("WEAPON:%s\n", monster->weapon);
      printf("ARMOR:%s\n", monster->armor);
      monster = monster->next;
    }
}

void	print_attack(int monster_id, int monster_hp)
{
  printf("Wolf %i has %i hp now.\n", monster_id, monster_hp);
}

void	print_attack_spe(int champ_id)
{
  printf("Super attack of %s.\n", champ->id);
}
